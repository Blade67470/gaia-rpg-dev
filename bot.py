import discord
from discord.ext import commands
from discord.ext.commands.cooldowns import BucketType
import traceback
import shelve
import math
import os
import classes
import functions
import random

import STATICS

bot = commands.Bot(command_prefix='!')
bot.remove_command('help')


@bot.event
async def on_ready():
    print('Ready to go!')
    await bot.change_presence(game=discord.Game(name=STATICS.PLAYING), status=discord.Status.dnd)


@bot.event
async def on_message(message):
    if message.content.startswith("!"):
        registered = functions.isRegistered(message)
        if registered:
            await bot.process_commands(message)
        elif (not registered) and (message.content not in ('!help', '!register', '!ping')):
            await bot.send_message(message.channel, 'go register')
        else:
            await bot.process_commands(message)


@bot.event
async def on_command_error(error, ctx):
    em = discord.Embed(title="Warning", description="{0}".format(error), color=discord.Color.red())
    await bot.send_message(ctx.message.channel, embed=em)


@bot.command(pass_context=True, aliases=["h"])
@commands.cooldown(1, 2, commands.BucketType.user)
async def help(ctx):
    hem = discord.Embed(title="❔ \u2009\u2009Help", description="Available commands:", color=discord.Color.blue())
    hem.add_field(name="Register", value="``!register <True/False>``\nRegisters a user.\nAdd the argument 'True' to enable Mobile mode.\nFalse by default.\nAlias: *none*\n", inline=False)
    hem.add_field(name="Ping", value="``!ping``\nSends a bot PM to private usage of the bot.\nAlias: *none*\n", inline=False)
    hem.add_field(name="Status", value="``!status``\nShows your Status info.\nAlias: ``!s``\n", inline=False)
    hem.add_field(name="Stats", value="``!stats``\nShows your Statistic values.\nAlias: ``!st``\n", inline=False)
    hem.add_field(name="Toggle Mobile", value="``!togglemobile``\nToggle the mobile mode.\nAlias: ``!tm``\n", inline=False)
    hem.add_field(name="Is Mobile", value="``!ismobile``\nShows if in mobile mode.\nAlias: ``!im``\n", inline=False)
    hem.add_field(name="Affinity", value="``!affinity``\nShows your magic affinities.\nAlias: ``!aff``\n", inline=False)
    hem.add_field(name="Fight", value="``!fight``\nStarts a fight against a monster.\nAlias: ``!fgt``\n", inline=False)
    hem.add_field(name="Attack", value="``!attack``\nLaunches an attack towards the monster you are fighting.\nAlias: ``!atk``\n", inline=False)
    await bot.send_message(ctx.message.channel, embed=hem)


@bot.command(pass_context=True)
@commands.cooldown(1, 2, commands.BucketType.user)
async def ping(ctx):
    await bot.send_message(ctx.message.author, content="Pong!")


@bot.command(pass_context=True)
@commands.cooldown(1, 2, commands.BucketType.user)
async def register(ctx, mobile=""):
    if not os.path.exists('databases/{0}'.format(ctx.message.author.id)):
        os.makedirs('databases/{0}'.format(ctx.message.author.id))
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        await bot.say("Registering user ''" + str(ctx.message.author.id + "'', please wait..."))
        if functions.isRegistered(ctx.message):
            await bot.say("User is already registered!")
        else:
            if not mobile == "":
                if mobile == "False":
                    if not functions.isRegistered(ctx.message):
                        db[ctx.message.author.id] = classes.User()
                        await bot.say("Successfully registered as *" + str(ctx.message.author.id) + "* !")
                    else:
                        await bot.say("Registering failed, please try again.")
                elif mobile == "True":
                    if not functions.isRegistered(ctx.message):
                        db[ctx.message.author.id] = classes.User()
                        db[ctx.message.author.id].mobile = True
                        await bot.say("Successfully registered as *" + str(ctx.message.author.id) + "* !")
                    else:
                        await bot.say("Registering failed, please try again.")
            else:
                if not functions.isRegistered(ctx.message):
                    db[ctx.message.author.id] = classes.User()
                    await bot.say("Successfully registered as *" + str(ctx.message.author.id) + "* !")
                else:
                    await bot.say("Registering failed, please try again.")


# @bot.command(pass_context=True)
# @commands.cooldown(1, 2, commands.BucketType.user)
# async def upgrade(ctx):
#     with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
#         a = db[ctx.message.author.id]
#         if "version" in a:
#             if a.version == STATICS.VERSION:
#                 vem = discord.Embed(title="❔ \u2009\u2009Version", description="The current version is {0}.\nYou are up to date!\n\n{1}".format(STATICS.VERSION, ctx.message.author.mention))
#                 await bot.send_message(ctx.message.channel, embed=vem)
#             else:
#                 a.version = STATICS.VERSION
#                 vem = discord.Embed(title="❔ \u2009\u2009Version", description="The current version is {0}.\nYou are *now* up to date!\n\n{1}".format(STATICS.VERSION, ctx.message.author.mention))
#                 await bot.send_message(ctx.message.channel, embed=vem)
#         else:
#             a.version = STATICS.VERSION
#             vem = discord.Embed(title="❔ \u2009\u2009Version", description="The current version is {0}.\nYou are *now* up to date!\n\n{1}".format(STATICS.VERSION, ctx.message.author.mention))
#             await bot.send_message(ctx.message.channel, embed=vem)


@bot.command(pass_context=True)
async def editmoney(ctx, arg0="", arg1="", arg2="", arg3=""):
    if ctx.message.author.server_permissions.administrator:
        with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
            if arg0 != "":
                if arg3 == "":
                    arg3 = ctx.message.author.id
                if arg0 == "add":
                    a = db[arg3]
                    a.money[arg2] += int(arg1)
                    db[arg3] = a
                    await bot.say("Adding "+str(arg1)+" "+str(arg2)+" to "+str(arg3)+"'s account!")
                elif arg0 == "remove":
                    a = db[arg3]
                    a.money[arg2] -= int(arg1)
                    db[arg3] = a
                    await bot.say("Removing "+str(arg1)+" "+str(arg2)+" to "+str(arg3)+"'s account!")
                elif arg0 == "set":
                    a = db[arg3]
                    a.money[arg2] = int(arg1)
                    db[arg3] = a
                    await bot.say("Setting "+str(arg3)+"'s "+str(arg2)+" to "+str(arg1))
            else:
                await bot.say("The correct usage for this command is:\n```!editmoney <add|remove|set> <amount> <gold|silver|copper> <ID>```")


@bot.command(pass_context=True)
async def dmg(ctx, arg0):
    functions.recalculateHp(ctx, arg=arg0)


@bot.command(pass_context=True, aliases=["s"])
@commands.cooldown(1, 2, commands.BucketType.user)
async def status(ctx):
    functions.recalculateExp(ctx)
    functions.recalculateMoney(ctx)
    functions.recalculateHp(ctx)
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        if not a.mobile:
            barlong = 50
        else:
            barlong = 20
        barshort = 20
        statusem = discord.Embed(title="⚙ Status", description="\u200B")

        statusem.add_field(name="✴ \u2009\u2009__Level__\u2009\u2009\u2009{0}{1}".format(str(STATICS.EMOTE_NONE),a.level), value=functions.bars(ctx, length=barlong, maxval=a.maxExp, val=a.exp, iscomplex=True, complextxt="Exp", addcomplextxt="Skill Point(s) left", addcomplexval=a.sp), inline=a.mobile)

        statusem.add_field(name="💰 \u2009\u2009__Gold__",               value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.money['gold'])), inline=True)
        statusem.add_field(name="{0} __Silver__".format(str(STATICS.EMOTE_NONE)), value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.money['silver'])), inline=True)
        statusem.add_field(name="{0} __Copper__".format(str(STATICS.EMOTE_NONE)), value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.money['copper'])), inline=True)

        statusem.add_field(name="❤ \u2009\u2009__Health__",   value=functions.bars(ctx, length=barlong, maxval=a.maxHealth, val=a.health), inline=a.mobile)
        statusem.add_field(name="🌀 \u2009\u2009__Mana__",    value=functions.bars(ctx, length=barshort, maxval=a.maxMana, val=a.mana), inline=True)
        statusem.add_field(name="⚡ \u2009\u2009__Stamina__",  value=functions.bars(ctx, length=barshort, maxval=a.maxStamina, val=a.stamina), inline=True)
        statusem.add_field(name="💤 \u2009\u2009__Fatigue__", value=functions.bars(ctx, length=barshort, maxval=a.maxFatigue, val=a.fatigue), inline=True)
        statusem.add_field(name="🍴 \u2009\u2009__Hunger__",  value=functions.bars(ctx, length=barshort, maxval=a.maxHunger, val=a.hunger), inline=True)

        await bot.send_message(ctx.message.channel, embed=statusem)


@bot.command(pass_context=True, aliases=["tm"])
@commands.cooldown(1, 2, commands.BucketType.user)
async def togglemobile(ctx):
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        a.mobile = not a.mobile
        db[ctx.message.author.id] = a
        tmem = discord.Embed(title="❔ \u2009\u2009Mobile", description="Mobile mode was set to {0}!".format(str(a.mobile)), color=discord.Color.blue())
        await bot.send_message(ctx.message.channel, embed=tmem)


@bot.command(pass_context=True, aliases=["im"])
@commands.cooldown(1, 2, commands.BucketType.user)
async def ismobile(ctx):
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        mem = discord.Embed(title="❔ \u2009\u2009Mobile", description="{0}\nMobile mode is {1}!".format(ctx.message.author.mention, str(a.mobile)), color=discord.Color.blue())
        await bot.send_message(ctx.message.channel, embed=mem)


@bot.command(pass_context=True, aliases=["st"])
@commands.cooldown(1, 2, commands.BucketType.user)
async def stats(ctx):
    functions.recalculateStats(ctx)
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        statsem = discord.Embed(title="⚙ Stats", description="\u200B")
        statsem.add_field(name="💪 \u2009\u2009__Strength__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.stats["strength"])), inline=True)
        statsem.add_field(name="🛡 \u2009\u2009__Defense__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.stats["defense"])), inline=True)
        statsem.add_field(name="🌪 \u2009\u2009__Speed__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.stats["speed"])), inline=True)
        statsem.add_field(name="💥 \u2009\u2009__Magic Attack__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.stats["attackMagic"])), inline=True)
        statsem.add_field(name="🔰 \u2009\u2009__Magic Defense__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.stats["defenseMagic"])), inline=True)
        statsem.add_field(name="💠 \u2009\u2009__Magic Special__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.stats["specialMagic"])), inline=True)

        await bot.send_message(ctx.message.channel, embed=statsem)


@bot.command(pass_context=True, aliases=["aff"])
@commands.cooldown(1, 2, commands.BucketType.user)
async def affinity(ctx):
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        affem = discord.Embed(title="⚙ Affinities", description="\u200B")
        affem.add_field(name="🔥 \u2009\u2009__Fire__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.magicAffinity['Fire'])), inline=True)
        affem.add_field(name="💧 \u2009\u2009__Water__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.magicAffinity['Water'])), inline=True)
        affem.add_field(name="🌿 \u2009\u2009__Earth__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.magicAffinity['Earth'])), inline=True)
        affem.add_field(name="🌬 \u2009\u2009__Air__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.magicAffinity['Air'])), inline=True)
        affem.add_field(name="🌑 \u2009\u2009__Darkness__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.magicAffinity['Darkness'])), inline=True)
        affem.add_field(name="☀ \u2009\u2009__Light__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.magicAffinity['Light'])), inline=True)
        if int(a.magicAffinity['Void']) > 0:
            affem.add_field(name="\u200B", value="\u200B", inline=True)
            affem.add_field(name="➰ \u2009\u2009__Void__", value="```\u2009 \u2009 \u2009 \u2009 {0}```".format(str(a.magicAffinity['Void'])), inline=True)
            affem.add_field(name="\u200B", value="\u200B", inline=True)

        await bot.send_message(ctx.message.channel, embed=affem)


@bot.command(pass_context=True)
async def setaffinity(ctx, affinity, value=0, usr=""):
    if (ctx.message.author.id == "174690047436652545") or (ctx.message.author.id == "255741395061571584") or ctx.message.author.server_permissions.administrator:
        if usr == "":
            usr = ctx.message.author.id
        with shelve.open('databases/{0}/{0}'.format(usr)) as db:
            if not affinity == "":
                a = db[usr]
                if affinity == "Void" or "Fire" or "Water" or "Air" or "Earth" or "Light" or "Darkness":
                    a.magicAffinity[affinity] = int(value)
                    db[usr] = a
                if affinity == "All":
                    a.magicAffinity["Void"] = int(value)
                    a.magicAffinity["Fire"] = int(value)
                    a.magicAffinity["Water"] = int(value)
                    a.magicAffinity["Air"] = int(value)
                    a.magicAffinity["Earth"] = int(value)
                    a.magicAffinity["Light"] = int(value)
                    a.magicAffinity["Darkness"] = int(value)
                    db[usr] = a
                else:
                    await bot.say("Invalid Affinity: {0}".format(str(affinity)))


@bot.command(pass_context=True)
async def setstats(ctx, stat, value=0, usr=""):
    if (ctx.message.author.id == "174690047436652545") or (ctx.message.author.id == "255741395061571584") or ctx.message.author.server_permissions.administrator:
        if usr == "":
            usr = ctx.message.author.id
        with shelve.open('databases/{0}/{0}'.format(usr)) as db:
            if not stat == "":
                a = db[usr]
                if stat == "strength" or "defense" or "speed" or "magicAttack" or "magicDefense" or "magicSpecial":
                    a.stats[stat] = int(value)
                    db[usr] = a
                if stat == "All":
                    a.stats["strength"] = int(value)
                    a.stats["defense"] = int(value)
                    a.stats["speed"] = int(value)
                    a.stats["attackMagic"] = int(value)
                    a.stats["defenseMagic"] = int(value)
                    a.stats["specialMagic"] = int(value)
                    db[usr] = a
                else:
                    await bot.say("Invalid Stat: {0}".format(str(stat)))


@bot.command(pass_context=True)
async def setsp(ctx, value=0, usr=""):
    if (ctx.message.author.id == "174690047436652545") or (ctx.message.author.id == "255741395061571584") or ctx.message.author.server_permissions.administrator:
        if usr == "":
            usr = ctx.message.author.id
        else:
            await bot.say("Invalid user.")
        with shelve.open('databases/{0}/{0}'.format(usr)) as db:
            a = db[usr]
            a.sp = value
            db[usr] = a


@bot.command(pass_context=True)
async def getid(ctx, user : discord.Member=None):
    if (ctx.message.author.id == "174690047436652545") or (ctx.message.author.id == "255741395061571584") or ctx.message.author.server_permissions.administrator:
        user = ctx.message.author if user is None else user
        return await bot.say("{0}'s id is:\n{1}".format(str(user), str(user.id)))


@bot.command(pass_context=True)
async def addexp(ctx, value=0, usr=""):
    if (ctx.message.author.id == "174690047436652545") or (ctx.message.author.id == "255741395061571584") or ctx.message.author.server_permissions.administrator:
        if usr == "":
            usr = ctx.message.author.id
        with shelve.open('databases/{0}/{0}'.format(usr)) as db:
            a = db[usr]
            a.exp += int(value)
            if a.exp >= a.maxExp:
                oldExp = a.maxExp
                a.maxExp = functions.recalculateExp(ctx)
                a.exp -= oldExp
                a.level += 1
                if (a.level % 5) == 0:
                    a.sp += 5
                else:
                    a.sp += 1
                a.stats["strength"] += functions.recalculateStats(ctx)
                a.stats["defense"] += functions.recalculateStats(ctx)
                a.stats["speed"] += functions.recalculateStats(ctx)
                a.stats["attackMagic"] += functions.recalculateStats(ctx)
                a.stats["defenseMagic"] += functions.recalculateStats(ctx)
                a.stats["specialMagic"] += functions.recalculateStats(ctx)
                lvlupem = discord.Embed(title="You leveled up!", description="{0}! You are now level {1}!".format(ctx.message.author.mention, a.level), color=discord.Color.blue())
                await bot.send_message(ctx.message.channel, embed=lvlupem)
            db[usr] = a


@bot.command(pass_context=True, aliases=["fgt"])
@commands.cooldown(1, 5, commands.BucketType.user)
async def fight(ctx):
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        rdm = random.randrange(0, 100, 1)
        if not a.inFight:
            if rdm > 10:
                if (rdm > 65) and (rdm < 95):
                    a.enemy = random.choice(classes.EnemyList(ctx).uncommon)
                elif (rdm > 95) and (rdm <= 100):
                    a.enemy = random.choice(classes.EnemyList(ctx).rare)
                else:
                    a.enemy = random.choice(classes.EnemyList(ctx).common)

                fgtem = discord.Embed(title="Opponent found!", description="A wild level {0} {1} {2} appeared!\n\n\n{3}".format(a.enemy["level"], a.enemy["rarity"], a.enemy["race"], ctx.message.author.mention), color=discord.Color.gold())
                fgtem.add_field(name="Health", value=functions.bars(ctx, length=18, maxval=a.enemy["maxHealth"], val=a.enemy["health"]), inline=False)
                fgtem.add_field(name="Strength", value="```{0}```".format(a.enemy["strength"]), inline=True)
                fgtem.add_field(name="Defense", value="```{0}```".format(a.enemy["defense"]), inline=True)
                fgtem.add_field(name="Speed", value="```{0}```".format(a.enemy["speed"]), inline=True)
                fgtem.add_field(name="Affinity", value="```{0}```".format("None"), inline=True)
                fgtem.set_thumbnail(url=a.enemy["url"])
                await bot.send_message(ctx.message.channel, embed=fgtem)
                a.inFight = True
                a.turn = 1
                db[ctx.message.author.id] = a
            else:
                await bot.say("No enemies to be seen.")
                a.inFight = False
                db[ctx.message.author.id] = a
        else:
            await bot.say("You are already in a fight!")


@bot.command(pass_context=True, aliases=["atk"])
@commands.cooldown(1, 2, commands.BucketType.user)
async def attack(ctx):
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        if a.stats["strength"] <= a.enemy["defense"]:
            dmg = a.stats["strength"]
        else:
            dmg = math.ceil((a.stats["strength"] - (a.enemy["defense"] / 2)))
        if a.inFight:
            if a.enemy["health"] > dmg:
                a.enemy["health"] -= dmg
                tem = discord.Embed(title="Turn {0}".format(a.turn), description="The wild level {0} {1} {2} lost {3} HP!".format(a.enemy["level"], a.enemy["rarity"], a.enemy["race"], dmg), color=discord.Color.gold())
                tem.set_thumbnail(url=a.enemy["url"])
                tem.add_field(name="Health", value="{0}".format(functions.bars(ctx, length=20, maxval=a.enemy["maxHealth"], val=a.enemy["health"])), inline=True)
                await bot.send_message(ctx.message.channel, embed=tem)
                a.turn += 1
                db[ctx.message.author.id] = a
            else:
                if a.enemy["rarity"] == "[Common]":
                    exp = max(10, random.randint((a.enemy["level"]*10), (a.enemy["level"]*40)))
                    copr = max(1, random.randint((a.enemy["level"]*1), (a.enemy["level"]*3)))
                elif a.enemy["rarity"] == "[Uncommon]":
                    exp = max(35, random.randint((a.enemy["level"]*35), (a.enemy["level"]*80)))
                    copr = max(3, random.randint((a.enemy["level"]*3), (a.enemy["level"]*10)))
                elif a.enemy["rarity"] == "[Rare]":
                    exp = max(80, random.randint((a.enemy["level"]*80), (a.enemy["level"]*200)))
                    copr = max(10, random.randint((a.enemy["level"]*10), (a.enemy["level"]*30)))
                elif a.enemy["rarity"] == "[Boss]":
                    exp = max(500, random.randint((a.enemy["level"]*500), (a.enemy["level"]*800)))
                    copr = max(30, random.randint((a.enemy["level"]*30), (a.enemy["level"]*200)))

                atkem = discord.Embed(title="Victory!", description="You defeated the wild level {0} {1} {2}!".format(a.enemy["level"], a.enemy["rarity"], a.enemy["race"]), color=discord.Color.gold())
                atkem.add_field(name="You obtained :", value="```{0}, {1}```".format(str(exp)+str(" Exp"), str(copr)+str(" Copper")), inline=False)
                atkem.add_field(name="\u200B", value="{0}".format(ctx.message.author.mention))
                atkem.set_thumbnail(url=a.enemy["url"])
                await bot.send_message(ctx.message.channel, embed=atkem)
                a.inFight = False
                a.exp += exp
                a.money['copper'] += copr
                functions.recalculateMoney(ctx)
                if a.exp >= a.maxExp:
                    oldExp = a.maxExp
                    a.maxExp = functions.recalculateExp(ctx)
                    a.exp -= oldExp
                    a.level += 1
                    if (a.level % 5) == 0:
                        a.sp += 5
                    else:
                        a.sp += 1
                    a.stats["strength"] += functions.recalculateStats(ctx)
                    a.stats["defense"] += functions.recalculateStats(ctx)
                    a.stats["speed"] += functions.recalculateStats(ctx)
                    a.stats["attackMagic"] += functions.recalculateStats(ctx)
                    a.stats["defenseMagic"] += functions.recalculateStats(ctx)
                    a.stats["specialMagic"] += functions.recalculateStats(ctx)
                    lvlupem = discord.Embed(title="You leveled up!", description="{0}! You are now level {1}!".format(ctx.message.author.mention, a.level), color=discord.Color.blue())
                    await bot.send_message(ctx.message.channel, embed=lvlupem)
                a.turn = 0
                db[ctx.message.author.id] = a
        else:
            await bot.say("You are not in a fight.")


bot.run(STATICS.TOKEN)
