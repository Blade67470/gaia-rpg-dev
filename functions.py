# -*- coding: utf-8 -*-
##
# Importing section
import shelve
import math
import os
##


def isRegistered(ctx):
    if not os.path.exists('databases/{0}'.format(ctx.author.id)):
        return False
    else:
        with shelve.open('databases/{0}/{0}'.format(str(ctx.author.id))) as db:
            if ctx.author.id in db:
                return True
            else:
                return False


def recalculateHp(ctx, arg=0):
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        a.health -= int(arg)
        if a.health < 0:
            a.health = 0
        elif a.health > a.maxHealth:
            a.health = a.maxHealth
        db[ctx.message.author.id] = a


def recalculateMoney(ctx):
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        while a.money['copper'] >= 100:
            a.money['silver'] += 1
            a.money['copper'] -= 100

        while a.money['silver'] >= 100:
            a.money['gold'] += 1
            a.money['silver'] -= 100

        db[ctx.message.author.id] = a


def recalculateExp(ctx):
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        newMaxExp = (a.maxExp * 2) + ((a.level + 1) * 100)

        return newMaxExp


def recalculateStats(ctx):
    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        sup = int(math.ceil(a.level / 2))
        return sup


def bars(context="", length=20, maxval=100, val=100, iscomplex=False, complextxt="", addcomplextxt="", addcomplexval=""):
    ctx = context

    with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
        a = db[ctx.message.author.id]
        if not a.mobile:
            if iscomplex:
                return "```╔{3}╗\n║{0}║\n╚{3}╝   \n {4} 〔{1}  ∕  {2}〕\u2009 \u2009  {5} {6}```".format(
                    ''.join(['█' for i in
                             range(length - int((maxval - val) / (maxval / length)))] +
                            ['–' for i in range(int((maxval - val) / (maxval / length)))]),
                    str(val), str(maxval),
                    ''.join(['═' for i in range(length)]), str(complextxt), str(addcomplexval),
                    str(addcomplextxt))
            else:
                return "```╔{3}╗\n║{0}║\n╚{3}╝   \n〔{1}  ∕  {2}〕```".format(
                    ''.join(['█' for i in
                             range(length - int((maxval - val) / (maxval / length)))] +
                            ['–' for i in range(int((maxval - val) / (maxval / length)))]),
                    str(val), str(maxval),
                    ''.join(['═' for i in range(length)]))
        else:
            if iscomplex:
                return "```╔{3}╗\n║{0}║\n╚{3}╝   \n {4} 〔{1}  ∕  {2}〕\n {5} {6}```".format(
                    ''.join(['█' for i in
                             range(length - int((maxval - val) / (maxval / length)))] +
                            ['░' for i in range(int((maxval - val) / (maxval / length)))]),
                    str(val), str(maxval),
                    ''.join(['═' for i in range(length)]), str(complextxt), str(addcomplexval),
                    str(addcomplextxt))
            else:
                return "```╔{3}╗\n║{0}║\n╚{3}╝   \n〔{1}  ∕  {2}〕```".format(
                    ''.join(['█' for i in
                             range(length - int((maxval - val) / (maxval / length)))] +
                            ['░' for i in range(int((maxval - val) / (maxval / length)))]),
                    str(val), str(maxval),
                    ''.join(['═' for i in range(length)]))
