# -*- coding: utf-8 -*-
##
# Importing section
import random
import shelve
import STATICS
##


class Weapon(object):
    ##
    def __init__(self, name="", description="", damage=1, protection=1):
        pass
        self.name = name
        self.description = description
        self.damage = damage
        self.protection = protection
        self.effects = Effects()
    ##


class User(object):
    ##
    def __init__(self):
        self.money = {'gold': 0, 'silver':0, 'copper':20}

        self.maxHealth = 100
        self.health = self.maxHealth
        self.maxMana = 100
        self.mana = self.maxMana
        self.fatigue = 0
        self.maxFatigue = 100
        self.maxStamina = 100
        self.stamina = self.maxStamina
        self.hunger = 0
        self.maxHunger = 100
        self.exp = 0
        self.maxExp = 125
        self.level = 1

        self.definedInventory = {'additionalTrinket': 0, 'back': 0, 'belt': 0,
                                'feet': 0, 'head': 0, 'leftArm': 0,
                                'leftHand': 0, 'leftHandTrinket': 0, 'legs': 0,
                                'mainHand': 0, 'necklace': 0, 'offHand': 0,
                                'rightArm': 0, 'rightHand': 0, 'rightHandTrinket': 0,
                                'torso': 0}
        self.undefinedInventory = [None] * 18

        self.stats = {"strength": random.randrange(1,5,1), "defense": random.randrange(1,5,1), "speed": random.randrange(1,5,1),
                      "attackMagic": random.randrange(1, 5, 1), "defenseMagic": random.randrange(1,5,1), "specialMagic": random.randrange(1,5,1)}

        if random.randrange(0,1000,1) == 666:
            randv = random.randrange(0,10,1)
        else:
            randv = 0

        self.magicAffinity = {'Fire': random.randrange(0,10,1), 'Water': random.randrange(0,10,1),
                              'Earth': random.randrange(0,10,1), 'Air': random.randrange(0,10,1),
                              'Light': random.randrange(0,10,1), 'Darkness': random.randrange(0,10,1),
                              'Void': randv}

        self.skills = []
        self.sp = 0

        self.skilled = {'health': 0, 'mana': 0, 'fatigue': 0, 'stamina': 0, 'hunger': 0, 'strength': 0,
                        'speed': 0, 'defense': 0, 'attackMagic': 0, 'defenseMagic': 0, 'specialMagic': 0}

        self.inFight = False
        self.enemy = ""
        self.enemyLevel = None
        self.enemyIsBoss = False
        self.isFirst = False
        self.turn = 0
        self.canAttack = False

        self.partyId = None
        self.inParty = False
        self.party = [None] * 5

        self.mobile = False

        self.version = STATICS.VERSION
    ##


class Effects(object):
    ##
    def __init__(self, poison=False, burn=False, freeze=False, petrify=False, bless=False, curse=False):
        pass
        self.poison = poison
        self.burn = burn
        self.freeze = freeze
        self.petrify = petrify
        self.bless = bless
        self.curse = curse
    ##


class EnemyList(object):
    ##
    def __init__(self, ctx):
        pass
        self.ctx = ctx
        with shelve.open('databases/{0}/{0}'.format(ctx.message.author.id)) as db:
            a = db[ctx.message.author.id]

        clevel = max(1, random.randrange((a.level - 4), a.level, 1))

        chealth = max(1, random.randint(2*clevel, 4*clevel))
        cstrength = max(1, random.randint((1 * round(clevel/2)), (3 * round(clevel/2))))
        cdefense = max(1, random.randint((1 * round(clevel/2)), (2 * round(clevel/2))))
        cspeed = max(1, random.randint((1 * round(clevel/2)), (2 * round(clevel/2))))

        ls_slime = {"rarity": "[Common]", "race": "Slime", "url": "https://puu.sh/xIGd6/ca92c1c27e.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_goblin = {"rarity": "[Common]", "race": "Goblin", "url": "https://puu.sh/xItGW/b02aad4c05.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_bandit = {"rarity": "[Common]", "race": "Bandit", "url": "https://puu.sh/xIIDb/54b792417e.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_boar = {"rarity": "[Common]", "race": "Boar", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_bear = {"rarity": "[Common]", "race": "Bear", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_wolf = {"rarity": "[Common]", "race": "Wolf", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_skeleton = {"rarity": "[Common]", "race": "Skeleton", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_spider = {"rarity": "[Common]", "race": "Spider", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_snake = {"rarity": "[Common]", "race": "Snake", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_gremlin = {"rarity": "[Common]", "race": "Gremlin", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        ls_kobold = {"rarity": "[Common]", "race": "Kobold", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": clevel, "maxHealth": chealth, "health": chealth, "strength": cstrength, "defense": cdefense, "speed": cspeed}
        self.common =   [ls_slime, ls_goblin, ls_bandit, ls_boar, ls_bear, ls_wolf, ls_skeleton, ls_spider,
                         ls_snake, ls_gremlin, ls_kobold]

        ulevel = max(1, random.randrange((a.level - 2), (a.level + 3), 1))

        uhealth = max(1, random.randint((3*round(ulevel/2)), (6*round(ulevel/2))))
        ustrength = max(1, random.randint((2*round(ulevel/2)), (4*round(ulevel/2))))
        udefense = max(1, random.randint((1*round(ulevel/2)), (3*round(ulevel/2))))
        uspeed = max(1, random.randint((2*round(ulevel/2)), (4*round(ulevel/2))))

        ls_gsnake = {"rarity": "[Uncommon]", "race": "Giant Snake", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": ulevel, "maxHealth": uhealth, "health": uhealth, "strength": ustrength, "defense": udefense, "speed": uspeed}
        ls_gant = {"rarity": "[Uncommon]", "race": "Giant Ant", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": ulevel, "maxHealth": uhealth, "health": uhealth, "strength": ustrength, "defense": udefense, "speed": uspeed}
        ls_grat = {"rarity": "[Uncommon]", "race": "Giant Rat", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": ulevel, "maxHealth": uhealth, "health": uhealth, "strength": ustrength, "defense": udefense, "speed": uspeed}
        ls_gbee = {"rarity": "[Uncommon]", "race": "Giant Bee", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": ulevel, "maxHealth": uhealth, "health": uhealth, "strength": ustrength, "defense": udefense, "speed": uspeed}
        ls_gscorpion = {"rarity": "[Uncommon]", "race": "Giant Scorpion", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": ulevel, "maxHealth": uhealth, "health": uhealth, "strength": ustrength, "defense": udefense, "speed": uspeed}
        ls_troll = {"rarity": "[Uncommon]", "race": "Troll", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": ulevel, "maxHealth": uhealth, "health": uhealth, "strength": ustrength, "defense": udefense, "speed": uspeed}
        ls_werewolf = {"rarity": "[Uncommon]", "race": "Werewolf", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": ulevel, "maxHealth": uhealth, "health": uhealth, "strength": ustrength, "defense": udefense, "speed": uspeed}
        ls_demon = {"rarity": "[Uncommon]", "race": "Demon", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": ulevel, "maxHealth": uhealth, "health": uhealth, "strength": ustrength, "defense": udefense, "speed": uspeed}
        self.uncommon = [ls_gsnake, ls_gant, ls_grat, ls_gbee, ls_gscorpion, ls_troll, ls_werewolf, ls_demon]

        rlevel = max(1, random.randrange((a.level + 1), (a.level + 10), 1))

        rhealth = max(1, random.randint((8*round(rlevel/2)), (14*round(rlevel/2))))
        rstrength = max(1, random.randint((4*round(rlevel/2)), (12*round(rlevel/2))))
        rdefense = max(1, random.randint((3*round(rlevel/2)), (6*round(rlevel/2))))
        rspeed = max(1, random.randint((4*round(rlevel/2)), (7*round(rlevel/2))))

        ls_golem = {"rarity": "[Rare]", "race": "Golem", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": rlevel, "maxHealth": rhealth, "health": rhealth, "strength": rstrength, "defense": rdefense, "speed": rspeed}
        ls_gryphon = {"rarity": "[Rare]", "race": "Gryphon", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": rlevel, "maxHealth": rhealth, "health": rhealth, "strength": rstrength, "defense": rdefense, "speed": rspeed}
        ls_dlord = {"rarity": "[Rare]", "race": "Demon Lord", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": rlevel, "maxHealth": rhealth, "health": rhealth, "strength": rstrength, "defense": rdefense, "speed": rspeed}
        self.rare = [ls_golem, ls_gryphon, ls_dlord]

        bhealth = random.randrange(40, 80, 1)
        bstrength = random.randrange(30, 65, 1)
        bdefense = random.randrange(20, 60, 1)
        bspeed = random.randrange(20, 40, 1)

        ls_dragon = {"rarity": "[Boss]", "race": "Dragon", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": 1, "maxHealth": bhealth, "health": bhealth, "strength": bstrength, "defense": bdefense, "speed": bspeed}
        ls_hydra = {"rarity": "[Boss]", "race": "Hydra", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": 1, "maxHealth": bhealth, "health": bhealth, "strength": bstrength, "defense": bdefense, "speed": bspeed}
        ls_angel = {"rarity": "[Boss]", "race": "Angel", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": 1, "maxHealth": bhealth, "health": bhealth, "strength": bstrength, "defense": bdefense, "speed": bspeed}
        ls_dking = {"rarity": "[Boss]", "race": "Demon King", "url": "https://puu.sh/xIusJ/65e3ea1235.png", "level": 1, "maxHealth": bhealth, "health": bhealth, "strength": bstrength, "defense": bdefense, "speed": bspeed}
        self.boss = [ls_dragon, ls_hydra, ls_angel, ls_dking]

    def common(self):
        return self.common

    def uncommon(self):
        return self.uncommon

    def rare(self):
        return self.rare

    def boss(self):
        return self.boss
